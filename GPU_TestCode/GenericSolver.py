try:
    import cupy as cp
    import numpy as np
except:
    import numpy as np
try:
    from numba import njit, guvectorize, cuda, vectorize,uint64,uint32,uint16,int32,int16,uint8, float32, int64
except: 
    pass
import matplotlib.pyplot as plt
import time as timer
import math
import pyopencl as cl
import pyopencl.array as clarray
import OpenCL_Implementation as ocl_imp

class FramerateOptions:
    def __init__(self, save_frames = False, framerate = 1, buffer_frames = True):
        try:
            assert(isinstance(save_frames,bool) and isinstance(framerate,int) and isinstance(buffer_frames,bool))
        except: raise(ValueError('Invalid argument types'))

        self.save_frames = save_frames
        self.framerate = framerate
        self.buffer_frames = buffer_frames


class GenericSolver:

    def __init__(self, K, stepsize=None, time=10, solver_type='triangle', base_dim=40, odd=True, scale=1, D_p=1, D_q=8,
                 C=4.5, frame_options=None, init=None, compute='CPU', precision='32'):
        self.framerate_options = frame_options

        self.stepsize = stepsize
        self.D_p = D_p
        self.D_q = D_q
        self.C = C
        self.framerate = self.framerate_options.framerate
        self.K = K
        self.scale = scale
        self.time = time
        self.solver_type = solver_type
        self.base_dim = base_dim
        self.odd = odd
        self.result = None
        assert issubclass(type(compute),str)
        self.compute = compute
        self.precision = precision

        try:
            self.precision = int(precision)
        except ValueError:
            if '32' in self.precision:
                self.precision = 32
            elif '64' in self.precision:
                self.precision = 64
            else:
                raise ValueError('Invalid Precision use 32 or 64 bit precision')

        if 'cpu' in compute.lower():
            self.compute = 'CPU'
        elif 'gpu' in compute.lower():
            self.compute = 'GPU'
        else:
            raise ValueError('Invalid Compute Argument')

        if ("tri" or "quart") in self.solver_type.lower():
            self.size = int(base_dim / 2 * scale) + int(odd)
        else:
            self.size = int(base_dim * scale) + int(odd)

        if self.stepsize is None:
            self.stepsize = 0.02212 * ((scale) ** -1.862) - 6.079e-6

            #Known optimal stepsizes#
            if self.scale in [0.5, 1, 2, 3, 4, 20, 40]:
                knownstepsizes = [0.015, 0.012, 0.006, 0.003, 0.0017, 7.7e-5, 1.9e-5]
                self.stepsize = knownstepsizes[
                    int(np.argwhere(np.array([0.5, 1, 2, 3, 4, 20, 40]) == self.scale)[0][0])]
        if issubclass(type(init), str):
            if "load" in init:
                pass
                # Fix This Later
        else:
            self.init = self.initializer()

        self.steps = int(np.ceil(self.time / self.stepsize))
        self.frames = int(np.ceil(self.time * self.framerate))
        self.final_image = self.mirror_triangle(self.init)

    def initializer(self):
        type = self.solver_type
        if "tri" in type.lower():
            C = self.C
            K = self.K
            init_q = np.zeros((self.size, self.size))
            init_p = np.zeros((self.size, self.size))
            init_q[0:int(self.base_dim * self.scale / 4 + 1), 0:int(self.base_dim * self.scale / 4 + 1)] = (K / C) + 0.2
            init_p[0:int(self.base_dim * self.scale / 4 + 1), 0:int(self.base_dim * self.scale / 4 + 1)] = C + 0.1
            init_p2 = np.triu(init_p)
            init_q2 = np.triu(init_q)
            init = np.array([init_p2, init_q2])
            s = self.size
            init_p = np.empty(int(s * (s + 1) / 2))
            init_q = np.empty(int(s * (s + 1) / 2))
            for i in range(s):
                l_start = int((s * (s + 1) - (s - i) * (s - i + 1)) / 2)
                l_end = int((s * (s + 1) - (s - i - 1) * (s - i - 1 + 1)) / 2)

                init_p[l_start:l_end] = init[0, i, i:s]
                init_q[l_start:l_end] = init[1, i, i:s]


            init = np.array([init_p, init_q])
            return init
        elif "quart" in type.lower():
            pass
        elif "square" in type.lower():
            pass

    def triangle_numba_solver(self, init, C, K, D_q, D_p, steps, stepsize, size, frames, laplace_accuracy=1):

        def calc_laplace_indices(size):
            s = int(size)

            indexmatrix = np.empty((s, s))
            tri_indices = np.triu_indices(s)
            indexmatrix[tri_indices] = np.arange(0, (s * (s + 1) / 2))
            indexmatrix = np.triu(indexmatrix)

            indexmatrix = indexmatrix + np.transpose(indexmatrix - np.diag(np.diag(indexmatrix)))
            left = np.zeros((4, int((s * (s + 1)) / 2)))
            right = np.zeros((4, int((s * (s + 1)) / 2)))
            upper = np.zeros((4, int((s * (s + 1)) / 2)))
            lower = np.zeros((4, int((s * (s + 1)) / 2)))

            for i in range(s):
                l_start = int((s * (s + 1) - (s - i) * (s - i + 1)) / 2)
                l_end = int((s * (s + 1) - (s - i - 1) * (s - i - 1 + 1)) / 2)
                for j in range(4):
                    if 1 + j - i > 0:
                        upper[j, l_start:l_end] = indexmatrix[1 + j - i, i:]


                    else:
                        upper[j, l_start:l_end] = indexmatrix[i - j - 1, i:]

                    if i + j + 1 > s - 1:
                        lower[j, l_start:l_end] = indexmatrix[s - (i + j) - 3, i:]

                    else:
                        lower[j, l_start:l_end] = indexmatrix[i + j + 1, i:]

                    if i > s - 4:
                        right1 = indexmatrix[i, i + j + 1:]
                        right2 = np.flip(indexmatrix[i, -2 - j:-1])
                        right2 = right2[-(s - i - len(right1)):]
                        right[j, l_start:l_end] = np.concatenate((right1, right2))


                    else:
                        right1 = indexmatrix[i, i + j + 1:]
                        right2 = np.flip(indexmatrix[i, -2 - j:-1])
                        right[j, l_start:l_end] = np.concatenate((right1, right2))

                    if i < 4:

                        if (j + 2 - i) > 0:
                            left1 = np.flip(indexmatrix[i, 1:j + 2 - i])
                            left2 = indexmatrix[i, max(i - j - 1, 0):-j - 1]
                            parts = np.concatenate((left1, left2))
                        else:
                            parts = indexmatrix[i, max(i - j - 1, 0):-j - 1]
                        left[j, l_start:l_end] = parts
                    else:
                        left[j, l_start:l_end] = indexmatrix[i, i - j - 1:-j - 1]

            return left.astype(int), right.astype(int), upper.astype(int), lower.astype(int)

        def calc_neumann_indices(size):
            s = size
            right_border = np.zeros(int(s))
            right_inside = np.zeros(int(s))
            upper_diag = np.zeros(int(s + 1))
            lower_diag = np.zeros(int(s + 1))
            for i in range(s):
                right_border[i] = ((s * (s + 1) - (s - i - 1) * (s - i)) / 2) - 1
                right_inside[i] = ((s * (s + 1) - (s - i - 1) * (s - i)) / 2) - 2
                upper_diag[i + 1] = int((s * (s + 1) - (s - i) * (s - i + 1)) / 2) + 3 * s + 1
                lower_diag[i] = i

            upper_diag[0] = 2 * s
            upper_diag[-1] = 2 * s - 1
            lower_diag[-1] = 2 * s - 1

            return right_border.astype(int), right_inside.astype(int), upper_diag.astype(int), lower_diag.astype(int)

        s = size
        dx2 = (20 / (s - 1)) ** 2
        left_shift, right_shift, up_shift, down_shift = calc_laplace_indices(size)

        right_border, right_inside, upper_diag, lower_diag = calc_neumann_indices(size)

        resultmemory = np.zeros((init.shape[0], init.shape[1], int(frames + 1)))
        f_i = init
        fdt_i = np.zeros((init.shape[0], init.shape[1]))


        k3, k2, k1, k0, kcenter = -(1 / 560), 8 / 315, -1 / 5, 8 / 5, 2 * -205 / 72
        c2, c1, c0, ccenter = 1 / 90, -3 / 20, 3 / 2, 2 * -49 / 18
        d1, d0, dcenter = -1 / 12, 4 / 3, 2 * -5 / 2


        @njit(parallel=True, fastmath=True)
        def compute_cpu(f_i, fdt_i, resultmemory):
            k = 0
            for i in range(steps - 1):

                f_i += stepsize * fdt_i
                f_i[:, right_border] = f_i[:, right_inside]

                if laplace_accuracy == 4:
                    laplacian = (k3*f_i[:,right_shift[3]] + k2*f_i[:,right_shift[2]] + k1*f_i[:,right_shift[1]] + k0*f_i[:,right_shift[0]] +k3*f_i[:,left_shift[3]] + k2*f_i[:,left_shift[2]] + k1*f_i[:,left_shift[1]] + k0*f_i[:,left_shift[0]] + k3*f_i[:,down_shift[3]] + k2*f_i[:,down_shift[2]] + k1*f_i[:,down_shift[1]] + k0*f_i[:,down_shift[0]] + k3*f_i[:,up_shift[3]] + k2*f_i[:,up_shift[2]] + k1*f_i[:,up_shift[1]] + k0*f_i[:,up_shift[0]] + kcenter*f_i) / dx2
                elif laplace_accuracy == 3:
                    laplacian = (c2*f_i[:,right_shift[2]] + c1*f_i[:,right_shift[1]] + c0*f_i[:,right_shift[0]] + c2*f_i[:,left_shift[2]] + c1*f_i[:,left_shift[1]] + c0*f_i[:,left_shift[0]] + c2*f_i[:,down_shift[2]] + c1*f_i[:,down_shift[1]] + c0*f_i[:,down_shift[0]] +  c2*f_i[:,up_shift[2]] + c1*f_i[:,up_shift[1]] + c0*f_i[:,up_shift[0]] + ccenter*f_i) / dx2
                elif laplace_accuracy == 2:
                    laplacian = (d1*f_i[:,right_shift[1]] + d0*f_i[:,right_shift[0]] + d1*f_i[:,left_shift[1]] + d0*f_i[:,left_shift[0]] +  d1*f_i[:,down_shift[1]] + d0*f_i[:,down_shift[0]] +  d1*f_i[:,up_shift[1]] + d0*f_i[:,up_shift[0]] + dcenter*f_i) / dx2
                else:
                    laplacian = ((f_i[:,right_shift[0]] + f_i[:, left_shift[0]] - 4 * f_i + f_i[:, down_shift[0]]  + f_i[:,up_shift[0]]) / dx2)


                p2q = f_i[0] ** 2 * f_i[1]
                fdt_i[0] = D_p * laplacian[0] + p2q + C - (K + 1) * f_i[0]
                fdt_i[1] = D_q * laplacian[1] - p2q + K * f_i[0]

                if i % int(steps / frames) == 0:
                    resultmemory[:, :, k] = f_i
                    k += 1
                    # print(((steps - i) / (steps / frames)) * (timer.time() - ETA))
                    norm = np.linalg.norm(fdt_i)

                    print(norm)

            resultmemory[:, :, -1] = f_i

        stepsize = np.float32(stepsize)
        laplacian = np.zeros_like(f_i,dtype=np.float32)
        dim = f_i.shape[1]
        border_dim = right_border.shape[0]


        f_i = f_i.astype(np.float32)
        fdt_i = fdt_i.astype(np.float32)
        resultmemory = resultmemory.astype(np.float32)
        start = timer.time()
        

        if 'GPU' in self.compute:
            try:
                resultmemory[0,:,-1], resultmemory[1,:,-1] = ocl_imp.calculate(f_i[0], f_i[1], right_inside, right_border, right_shift[0], left_shift[0], up_shift[0], down_shift[0], K, C, self.time, self.precision)
            except AttributeError:
                if self.precision == 64:
                    resultmemory[0,:,-1], resultmemory[1,:,-1] = ocl_imp.calculate(f_i[0], f_i[1], right_inside, right_border, right_shift[0], left_shift[0], up_shift[0], down_shift[0], K, C, self.time, 32)
                
        elif 'CPU' in self.compute:
            compute_cpu(f_i,fdt_i,resultmemory)


        #compute(f_i, fdt_i, resultmemory)
        print('Elapsed : ', timer.time() - start)
        return resultmemory

    def mirror_triangle(self, M):

        M, s = self.unflatten_triangle(M)
        result = np.empty((2, (s - 1) * 2 + 1, (s - 1) * 2 + 1))
        result[0, s - 1:, s - 1:] = M[0, :, :] + np.transpose(M[0, :, :]) - np.diag(np.diag(M[0, :, :]))
        result[1, s - 1:, s - 1:] = M[1, :, :] + np.transpose(M[1, :, :]) - np.diag(np.diag(M[1, :, :]))
        result[:, :s, s - 1:] = np.flip(result[:, s - 1:, s - 1:], 1)
        result[:, :, :s] = np.flip(result[:, :, s - 1:], -1)
        return result

    def unflatten_triangle(self, M):

        if np.ndim(M) > 2:
            return M, M.shape[1]
        else:
            s = int(np.floor(np.sqrt(2 * M.shape[1])))
            result = np.zeros((2, s, s))
            for i in range(s):
                l_start = int((s * (s + 1) - (s - i) * (s - i + 1)) / 2)
                l_end = int((s * (s + 1) - (s - i - 1) * (s - i - 1 + 1)) / 2)
                result[:, i, i:s] = M[:, l_start:l_end]
            return result, s

    def solve(self):
        if "tri" in self.solver_type.lower():

            self.result = self.triangle_numba_solver(self.init, self.C, self.K, self.D_q, self.D_p, self.steps,
                                                     self.stepsize, self.size, self.frames)
            self.final_image = self.mirror_triangle(self.result[:, :, -1])

        elif "square" in self.solver_type.lower():
            pass
        elif "quart" in self.solver_type.lower():
            pass
        pass

    def plot(self):
        plt.figure()
        plt.imshow(self.final_image[0], cmap='terrain')
        plt.colorbar()
        # plt.imshow(self.final_image[0],cmap='terrain')


