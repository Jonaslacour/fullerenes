from GenericSolver import *

f_options = FramerateOptions(True,10,False)

SolverObj = GenericSolver(K=7,time=1,scale=40,frame_options=f_options,compute='GPU', precision=32)




SolverObj.solve()
SolverObj.plot()

plt.show()

