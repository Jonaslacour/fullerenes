import numpy as np
import pyopencl as cl
import pyopencl.array as cl_array
import os



kernel = """

__kernel void integrate(
    __global float* P,
    __global float* Q,
    __global float* P_copy,
    __global float* Q_copy,
    __global int* rborder,
    __global int* rinside,
    __global int* right,
    __global int* left,
    __global int* up,
    __global int* down,
    const unsigned int arraysize,
    const float K,
    const float C,
    const float t,
    const float C1,
    const float C2,
    const float C3,
    const float stepsize,
    const unsigned int steps,
    const unsigned int side_length
)
{   
    unsigned int tid = get_global_id(0);
    float R3, R4, R6, dPdt, dQdt;
    unsigned int R0, R1;
    

    P_copy[tid] = P[tid];
    Q_copy[tid] = Q[tid];

    barrier(CLK_GLOBAL_MEM_FENCE);

    if (tid <= side_length) {

        R0 = rinside[tid];
        R1 = rborder[tid];

        P[R1] = P[R0];
        Q[R1] = Q[R1];
    }
    
    

    R3 = P_copy[tid];
    R4 = Q_copy[tid];

    R6 = R3 * R3 * R4;
    
    dPdt = C2 * (P_copy[right[tid]] + P_copy[left[tid]] + P_copy[down[tid]] + P_copy[up[tid]]) + R6 + C - C1* R3;

    dQdt = C3 * (Q_copy[right[tid]] + Q_copy[left[tid]] - 4.0f * R4 + Q_copy[down[tid]] + Q_copy[up[tid]]) - R6 + K * R3 ;
    
    barrier(CLK_GLOBAL_MEM_FENCE);

    P[tid] += dPdt*stepsize;
    Q[tid] += dQdt*stepsize; 
}

__kernel void copy(
                        __global float* P,
                        __global float* Q,
                        __global float* P_copy,
                        __global float* Q_copy
)
{
    unsigned int tid = get_global_id(0);
    P_copy[tid] = P[tid];
    Q_copy[tid] = Q[tid];
}


__kernel void neumann(
                            __global float* P,
                            __global float* Q,
                            __global int* rborder,
                            __global int* rinside
)
{
    unsigned int tid = get_global_id(0);
    unsigned int R0 = rinside[tid];
    unsigned int R1 = rborder[tid];

    P[R1] = P[R0];
    Q[R1] = Q[R1];
}
"""

def calculate(in_P, in_Q, in_rinside, in_rborder, in_right, in_left, in_up, in_down, K, C, t, precision):
    
    if (precision == '32') or (precision == 32):
        float_type = np.single
        new_kernel = kernel.replace('double','float')
    elif (precision == '64') or (precision == 64):
        float_type = np.double
        new_kernel = kernel.replace('float','double')
    else:
        raise(ValueError('Invalid Precision: Choose 32 or 64 bit precision'))   

    platforms = cl.get_platforms()
    print("\nNumber of OpenCL platforms:",  len(platforms))
    device = cl.get_platforms()[0].get_devices()[0]
    context = cl.Context([device])  
    queue = cl.CommandQueue(context)
    options = ['-cl-mad-enable', '-cl-no-signed-zeros', '-cl-unsafe-math-optimizations', '-cl-finite-math-only', '-cl-fast-relaxed-math']
    program = cl.Program(context, kernel).build(options=options)

    
    P = in_P.astype(float_type)
    Q = in_Q.astype(float_type)
    P_copy = np.copy(P).astype(float_type)
    Q_copy = np.copy(Q).astype(float_type)

    rborder = in_rborder.astype(np.int32)
    rinside = in_rinside.astype(np.int32)

    right = in_right.astype(np.int32)
    left = in_left.astype(np.int32)
    up = in_up.astype(np.int32)
    down = in_down.astype(np.int32)
    
    d_rinside = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=rinside)
    d_rborder = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=rborder)
    d_right = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf =right)
    d_left = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf = left)
    d_up = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf =up)
    d_down = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf = down)
    d_P = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf = P)
    d_Q = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf = Q)
    d_P_copy = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf = P_copy)
    d_Q_copy = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf = Q_copy)
    
    s = np.sqrt(1.0 + 8.0*(P.size))/2 - 1/2

    print(s)
    stepsize = 0.02212 * np.power((s - 1) / 20, -1.862) - 6.079e-6
    steps = int(t / stepsize)
    dx = (20.0 / (s - 1.0)) * (20.0 / (s - 1.0))
    C2 = 1.0 / dx
    C1 = K + 1.0 + C2 * 4.0
    C3 = 8.0 / dx
    
    integrate = program.integrate
    copy = program.copy
    neumann = program.neumann
    copy.set_scalar_arg_dtypes([None, None, None, None])
    neumann.set_scalar_arg_dtypes([None, None, None, None])
    integrate.set_scalar_arg_dtypes([None, None, None, None, None, None, None, None, None, None, np.uint32, float_type, float_type, float_type, float_type, float_type, float_type, float_type, np.uint32, np.uint32])
    
    
 
    for i in range(steps):

        #copy(queue, P.shape, None, d_P, d_Q, d_P_copy, d_Q_copy)
        integrate(queue, P.shape, None, d_P, d_Q, d_P_copy, d_Q_copy, d_rborder, d_rinside, d_right, d_left, d_up, d_down, int(P.size), K, C, t, C1, C2, C3,stepsize, steps,int(s))
        #neumann(queue, (int(s),), None, d_P, d_Q, d_rborder, d_rinside)
        if(i % 1000 == 0):
            queue.finish()
            queue.flush()
    
    
    cl.enqueue_barrier(queue)
    cl.enqueue_copy(queue, P, d_P)
    cl.enqueue_copy(queue, Q, d_Q)
    queue.finish()

    return P, Q




