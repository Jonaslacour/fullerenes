import cupy as np
import numpy as numpy
import time as timer
import matplotlib.pyplot as plt

def initialize_triangle(resolution, params):
    C = params[2]
    K = params[3]
    init_q = np.zeros((int(20 * resolution +1), int(20 * resolution +1)))
    init_p = np.zeros((int(20 * resolution +1), int(20 * resolution +1)))
    init_q[0:int(10 * resolution +1), 0:int(10 * resolution +1)] = (K / C) + 0.2
    init_p[0:int(10 * resolution +1), 0:int(10 * resolution +1)] = C + 0.1
    init_p2 = np.triu(init_p)
    init_q2 = np.triu(init_q)
    return np.array([init_p2, init_q2])

def initialize_flatten_triangle(resolution, params):
    s = int(20*resolution +1)
    init = initialize_triangle(resolution, params)
    init_p = np.empty(int(s*(s+1)/2))
    init_q = np.empty(int(s*(s+1)/2))
    for i in range(s):
        l_start = int((s * (s + 1) - (s - i) * (s - i + 1)) / 2)
        l_end = int((s * (s + 1) - (s - i - 1) * (s - i - 1 + 1)) / 2)

        init_p[l_start:l_end] = init[0,i,i:s]

        init_q[l_start:l_end] = init[1,i,i:s]

    init = np.array([init_p,init_q])
    return init




def triangle_flattened_forward_euler(time, params, res, accuracy=1,stepsize=None, initial=None, frames=10):
    if initial is None:
        initial = initialize_flatten_triangle(res,params)

    if stepsize is None:
        stepsize = 0.02212 * ((res) ** -1.862) - 6.079e-6

        # ==== Known optimal stepsizes===#
        if res in [0.5, 1, 2, 3, 4, 20, 40]:
            knownstepsizes = [0.015, 0.012, 0.006, 0.003, 0.0017, 7.7e-5, 1.9e-5]
            print(np.argwhere(np.array([0.5, 1, 2, 3, 4, 20, 40]) == res)[0][0])
            stepsize = knownstepsizes[int(np.argwhere(np.array([0.5, 1, 2, 3, 4, 20, 40]) == res)[0][0])]

    print('Stepsize: ','{:.2e}'.format(stepsize))
    stepsize = stepsize
    steps = int(np.ceil(time / stepsize))
    s = int((res*20) +1)
    dx2 = (20 / (s - 1)) ** 2
    C = params[2]

    D_p = params[0]
    D_q = params[1]
    K = params[3]
    left_shift,right_shift,up_shift,down_shift = calc_laplace_indices(res)

    right_border, right_inside, upper_diag, lower_diag = calc_neumann_indices(res)
    resultmemory = np.zeros((initial.shape[0], initial.shape[1], int(frames + 1)))
    f_i = initial.astype('float32')
    fdt_i = np.zeros((initial.shape[0], initial.shape[1])).astype('float32')
    print(f_i.dtype)
    k = 0

    k3, k2, k1, k0, kcenter = -(1/560),8/315,-1/5,8/5, 2*-205/72
    c2, c1, c0, ccenter = 1/90,-3/20,3/2,2*-49/18
    d1, d0, dcenter = -1/12, 4/3, 2*-5/2
    start = timer.time()
    ETA = timer.time()
    for i in range(steps - 1):

        f_i += stepsize * fdt_i
        f_i[:, right_border] = f_i[:, right_inside]

        if accuracy == 4:
            laplacian = (k3*f_i[:,right_shift[3]] + k2*f_i[:,right_shift[2]] + k1*f_i[:,right_shift[1]] + k0*f_i[:,right_shift[0]] +k3*f_i[:,left_shift[3]] + k2*f_i[:,left_shift[2]] + k1*f_i[:,left_shift[1]] + k0*f_i[:,left_shift[0]] + k3*f_i[:,down_shift[3]] + k2*f_i[:,down_shift[2]] + k1*f_i[:,down_shift[1]] + k0*f_i[:,down_shift[0]] + k3*f_i[:,up_shift[3]] + k2*f_i[:,up_shift[2]] + k1*f_i[:,up_shift[1]] + k0*f_i[:,up_shift[0]] + kcenter*f_i) / dx2
        elif accuracy == 3:
            laplacian = (c2*f_i[:,right_shift[2]] + c1*f_i[:,right_shift[1]] + c0*f_i[:,right_shift[0]] + c2*f_i[:,left_shift[2]] + c1*f_i[:,left_shift[1]] + c0*f_i[:,left_shift[0]] + c2*f_i[:,down_shift[2]] + c1*f_i[:,down_shift[1]] + c0*f_i[:,down_shift[0]] +  c2*f_i[:,up_shift[2]] + c1*f_i[:,up_shift[1]] + c0*f_i[:,up_shift[0]] + ccenter*f_i) / dx2
        elif accuracy == 2:
            laplacian = (d1*f_i[:,right_shift[1]] + d0*f_i[:,right_shift[0]] + d1*f_i[:,left_shift[1]] + d0*f_i[:,left_shift[0]] +  d1*f_i[:,down_shift[1]] + d0*f_i[:,down_shift[0]] +  d1*f_i[:,up_shift[1]] + d0*f_i[:,up_shift[0]] + dcenter*f_i) / dx2
        else:


            laplacian = ((f_i[:,right_shift[0]] + f_i[:, left_shift[0]] - 4 * f_i + f_i[:, down_shift[0]]  + f_i[:,up_shift[0]]) / dx2)




        p2q = f_i[0]**2 * f_i[1]
        fdt_i[0] = D_p * laplacian[0] + p2q + C - (K + 1) * f_i[0]
        fdt_i[1] = D_q * laplacian[1] - p2q + K * f_i[0]

        if i % int(steps / frames) == 0:
            resultmemory[:, :,  k] = f_i
            print(p2q.dtype)
            k += 1
            print(((steps - i) / (steps / frames)) * (timer.time() - ETA))
            norm = np.linalg.norm(fdt_i)
            try:
                print('Norm is', "{:.2e}".format(norm.get()))
            except:
                print('Norm is', "{:.2e}".format(norm))
            ETA = timer.time()
    resultmemory[:, :, -1] = f_i
    print('Elapsed time: ', timer.time() - start)
    return resultmemory


def numpy_triangle_flattened_forward_euler(time, params, res, accuracy=1,stepsize=None, initial=None, frames=10):
    print('USING NUMPY')
    if initial is None:
        initial = initialize_flatten_triangle(res,params)

    if stepsize is None:
        stepsize = 0.02212 * ((res) ** -1.862) - 6.079e-6

        # ==== Known optimal stepsizes===#
        if res in [0.5, 1, 2, 3, 4, 20, 40]:
            knownstepsizes = [0.015, 0.012, 0.006, 0.003, 0.0017, 7.7e-5, 1.9e-5]
            print(numpy.argwhere(numpy.array([0.5, 1, 2, 3, 4, 20, 40]) == res)[0][0])
            stepsize = knownstepsizes[int(numpy.argwhere(np.array([0.5, 1, 2, 3, 4, 20, 40]) == res)[0][0])]

    print('Stepsize: ','{:.2e}'.format(stepsize))

    steps = int(np.ceil(time / stepsize))
    s = int((res*20) +1)
    dx2 = (20 / (s - 1)) ** 2
    C = params[2]

    D_p = params[0]
    D_q = params[1]
    K = params[3]
    left_shift,right_shift,up_shift,down_shift = calc_laplace_indices(res)
    left_shift,right_shift,up_shift,down_shift = left_shift.get(),right_shift.get(),up_shift.get(),down_shift.get()



    right_border, right_inside, upper_diag, lower_diag = calc_neumann_indices(res)

    right_border, right_inside, upper_diag, lower_diag = right_border.get(), right_inside.get(), upper_diag.get(), lower_diag.get()

    resultmemory = numpy.zeros((initial.shape[0], initial.shape[1], int(frames + 1)))
    f_i = initial.get().astype('float32')
    fdt_i = numpy.zeros((initial.shape[0], initial.shape[1])).astype('float32')

    k = 0

    k3, k2, k1, k0, kcenter = -(1/560),8/315,-1/5,8/5, 2*-205/72
    c2, c1, c0, ccenter = 1/90,-3/20,3/2,2*-49/18
    d1, d0, dcenter = -1/12, 4/3, 2*-5/2


    start = timer.time()
    ETA = timer.time()
    for i in range(steps - 1):

        f_i += stepsize * fdt_i
        f_i[:, right_border] = f_i[:, right_inside]

        if accuracy == 4:
            laplacian = (k3*f_i[:,right_shift[3]] + k2*f_i[:,right_shift[2]] + k1*f_i[:,right_shift[1]] + k0*f_i[:,right_shift[0]] +k3*f_i[:,left_shift[3]] + k2*f_i[:,left_shift[2]] + k1*f_i[:,left_shift[1]] + k0*f_i[:,left_shift[0]] + k3*f_i[:,down_shift[3]] + k2*f_i[:,down_shift[2]] + k1*f_i[:,down_shift[1]] + k0*f_i[:,down_shift[0]] + k3*f_i[:,up_shift[3]] + k2*f_i[:,up_shift[2]] + k1*f_i[:,up_shift[1]] + k0*f_i[:,up_shift[0]] + kcenter*f_i) / dx2
        elif accuracy == 3:
            laplacian = (c2*f_i[:,right_shift[2]] + c1*f_i[:,right_shift[1]] + c0*f_i[:,right_shift[0]] + c2*f_i[:,left_shift[2]] + c1*f_i[:,left_shift[1]] + c0*f_i[:,left_shift[0]] + c2*f_i[:,down_shift[2]] + c1*f_i[:,down_shift[1]] + c0*f_i[:,down_shift[0]] +  c2*f_i[:,up_shift[2]] + c1*f_i[:,up_shift[1]] + c0*f_i[:,up_shift[0]] + ccenter*f_i) / dx2
        elif accuracy == 2:
            laplacian = (d1*f_i[:,right_shift[1]] + d0*f_i[:,right_shift[0]] + d1*f_i[:,left_shift[1]] + d0*f_i[:,left_shift[0]] +  d1*f_i[:,down_shift[1]] + d0*f_i[:,down_shift[0]] +  d1*f_i[:,up_shift[1]] + d0*f_i[:,up_shift[0]] + dcenter*f_i) / dx2
        else:
            laplacian = ((f_i[:,right_shift[0]] + f_i[:, left_shift[0]] - 4 * f_i + f_i[:, down_shift[0]]  + f_i[:,up_shift[0]]) / dx2)




        p2q = f_i[0]**2 * f_i[1]
        fdt_i[0] = D_p * laplacian[0] + p2q + C - (K + 1) * f_i[0]
        fdt_i[1] = D_q * laplacian[1] - p2q + K * f_i[0]

        if i % int(steps / frames) == 0:
            resultmemory[:, :,  k] = f_i
            k += 1
            print(((steps - i) / (steps / frames)) * (timer.time() - ETA))
            norm = np.linalg.norm(fdt_i)
            try:
                print('Norm is', "{:.2e}".format(norm.get()))
            except:
                print('Norm is', "{:.2e}".format(norm))
            ETA = timer.time()
    resultmemory[:, :, -1] = f_i
    print('Elapsed time: ', timer.time() - start)
    return resultmemory





def calc_laplace_indices(res):
    s = int((res * 20)+1)

    indexmatrix = np.empty((s,s))
    tri_indices = numpy.triu_indices(s)
    indexmatrix[tri_indices] = np.arange(0,(s*(s+1)/2))
    indexmatrix = np.triu(indexmatrix)

    indexmatrix = indexmatrix + np.transpose(indexmatrix - np.diag(np.diag(indexmatrix)))
    left = np.zeros((4,int((s * (s + 1)) / 2)))
    right = np.zeros((4, int((s * (s + 1)) / 2)))
    upper = np.zeros((4, int((s * (s + 1)) / 2)))
    lower = np.zeros((4, int((s * (s + 1)) / 2)))
    for i in range(s):
        l_start = int((s * (s + 1) - (s - i) * (s - i + 1)) / 2)
        l_end = int((s * (s + 1) - (s - i - 1) * (s - i - 1 + 1)) / 2)
        for j in range(4):
            if 1+j-i > 0:
                upper[j,l_start:l_end] = indexmatrix[1 + j - i, i:]

            else:
                upper[j,l_start:l_end] = indexmatrix[i-j-1,i:]


            if i+j+1 > s-1:
                lower[j,l_start:l_end] = indexmatrix[s-(i+j)-3,i:]
            else:
                lower[j,l_start:l_end] = indexmatrix[i+j+1,i:]

            if i > s-4:
                right1 = indexmatrix[i, i + j + 1:]
                right2 = np.flip(indexmatrix[i, -2 - j:-1])
                right2 = right2[-(s-i-len(right1)):]
                right[j, l_start:l_end] = np.concatenate((right1, right2))
            else:
                right1 = indexmatrix[i, i + j + 1:]
                right2 = np.flip(indexmatrix[i, -2 - j:-1])
                right[j, l_start:l_end] = np.concatenate((right1, right2))
            if i < 4:

                if (j + 2 -i) > 0:
                    left1 = np.flip(indexmatrix[i, 1:j + 2 - i])
                    left2 = indexmatrix[i, max(i - j - 1, 0):-j - 1]
                    parts = np.concatenate((left1,left2))
                else:
                    parts = indexmatrix[i, max(i - j - 1, 0):-j - 1]
                left[j, l_start  :l_end] = parts
            else:
                left[j, l_start :l_end] = indexmatrix[i, i-j-1:-j - 1]

    return left.astype('int32'),right.astype('int32'),upper.astype('int32'),lower.astype('int32')
def calc_neumann_indices(res):
    s = int((res * 20)+1)
    right_border = np.zeros(int(s))
    right_inside = np.zeros(int(s))
    upper_diag = np.zeros(int(s + 1))
    lower_diag = np.zeros(int(s + 1))
    for i in range(s):
        right_border[i] = ((s * (s + 1) - (s - i - 1) * (s - i )) / 2) - 1
        right_inside[i] = ((s * (s + 1) - (s - i - 1) * (s - i )) / 2) - 2
        upper_diag[i+1] = int((s * (s + 1) - (s - i  ) * (s - i  + 1)) / 2) + 3*s + 1
        lower_diag[i] = i


    upper_diag[0] = 2*s
    upper_diag[-1] = 2*s-1
    lower_diag[-1] = 2*s -1

    return right_border.astype('int32'),right_inside.astype('int32'), upper_diag.astype('int32'), lower_diag.astype('int32')
def mirror_triangle(M):
    try:
        M,s = unflatten_triangle(M)
        result = np.empty((2, (s - 1) * 2 + 1, (s - 1) * 2 + 1))
        result[0, s - 1:, s - 1:] = M[0, :, :] + np.transpose(M[0, :, :]) - np.diag(np.diag(M[0, :, :]))
        result[1, s - 1:, s - 1:] = M[1, :, :] + np.transpose(M[1, :, :]) - np.diag(np.diag(M[1, :, :]))
        result[:, :s, s - 1:] = np.flip(result[:, s - 1:, s - 1:], 1)
        result[:, :, :s] = np.flip(result[:, :, s - 1:], -1)
    except:
        M, s = unflatten_triangle(M)
        result = numpy.empty((2, (s - 1) * 2 + 1, (s - 1) * 2 + 1))
        result[0, s - 1:, s - 1:] = M[0, :, :] + numpy.transpose(M[0, :, :]) - numpy.diag(numpy.diag(M[0, :, :]))
        result[1, s - 1:, s - 1:] = M[1, :, :] + numpy.transpose(M[1, :, :]) - numpy.diag(numpy.diag(M[1, :, :]))
        result[:, :s, s - 1:] = numpy.flip(result[:, s - 1:, s - 1:], 1)
        result[:, :, :s] = numpy.flip(result[:, :, s - 1:], -1)
    return result


def unflatten_triangle(M):
    try:
        if np.ndim(M) > 2:
            return M, M.shape[1]
        else:
            s = int(np.floor(np.sqrt(2*M.shape[1])))
            result = np.zeros((2,s,s))
            for i in range(s):
                l_start = int((s * (s + 1) - (s - i) * (s - i + 1)) / 2)
                l_end = int((s * (s + 1) - (s - i - 1) * (s - i - 1 + 1)) / 2)
                result[:,i,i:s] = M[:,l_start:l_end]
            return result, s
    except:
        if numpy.ndim(M) > 2:
            return M, M.shape[1]
        else:
            s = int(numpy.floor(numpy.sqrt(2*M.shape[1])))
            result = numpy.zeros((2,s,s))
            for i in range(s):
                l_start = int((s * (s + 1) - (s - i) * (s - i + 1)) / 2)
                l_end = int((s * (s + 1) - (s - i - 1) * (s - i - 1 + 1)) / 2)
                result[:,i,i:s] = M[:,l_start:l_end]
            return result, s



def solver(time,res,K,accuracy=1,stepsize=None,init=None,frames=10):
    if res > 7:
        result = triangle_flattened_forward_euler(time=time, params=[1, 8, 4.5, K], accuracy=accuracy, res=res, stepsize=stepsize, initial=init,frames=frames)
    else:
        result = numpy_triangle_flattened_forward_euler(time=time, params=[1, 8, 4.5, K], accuracy=accuracy, res=res,stepsize=stepsize, initial=init,frames=frames)
    return result

#Invoke solver:
result = solver(0.1,50,7,frames=10)
#result = numpy_triangle_flattened_forward_euler(time=1,params=[1, 8, 4.5, 12],accuracy=1,res=15,stepsize=None)

#Transform result:


test_array = numpy.loadtxt('C:/Users/Jonas/Documents/Scientific Computing/P.txt', delimiter=', ')


transform = mirror_triangle(numpy.array([test_array,test_array]))
print(test_array.shape)
#plot [0] = P , [1] = Q ,  Matplotlib can't plot CuPy arrays so you need to convert through .get() call
try:
    plt.imshow(transform[0],cmap='terrain')
except TypeError:
    plt.imshow(transform[0].get(),cmap='terrain')

plt.show()