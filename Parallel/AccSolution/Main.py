from Fullerene_GeometryTest import energy
import unittest 
import numpy as np
import numpy.linalg as la
from numpy import array, gradient, pi, cos, result_type, sin, sqrt, linspace
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import struct


import time

from Fullerene_GeometryTest import *


from IPython.core.display import display, HTML
display(HTML("<style>.container { width:100% !important; }</style>")) 

np.random.seed(22)

NA = np.newaxis

from C60ih import *
#print(points_opt.shape)
#print(cubic_neighbours.shape)
#print(pentagons.shape)
#print(hexagons.shape)


#print('DFT m062x optimized geometry')

## General force parameters
R0,ang0,dih0,ang0_p,ang0_m,dih_a,dih_m,dih_p,fR,fang,fdih,fang_p,fang_m,fdih_a,fdih_m,fdih_p = Parameters_from_geometry(face_right,cubic_neighbours,True)

## Seminario Force Parameters, for C60Ih
#R0,ang0,dih0,ang0_p,ang0_m,dih_a,dih_m,dih_p,fR,fang,fdih,fang_p,fang_m,fdih_a,fdih_m,fdih_p = Parameters_from_geometry(face_right,cubic_neighbours,True)

k0 = array([R0,ang0,dih0,ang0_p,ang0_m,dih_a,dih_m,dih_p])
f0 = array([fR,fang,fdih,fang_p,fang_m,fdih_a,fdih_m,fdih_p])

def conjugate_gradient(X, neighbours,next_on_face, prev_on_face,k0,f0, N_max,UseAll): 

    d = -gradient(X,neighbours,next_on_face, prev_on_face,k0,f0,UseAll)
    r0 = d; #initial residual 
    d /= la.norm(d) # Normalized initial search direction
    #Save geometries for plotting
    xGeom = np.empty(0)  
    
    dnorm = la.norm(d) #initial dnorm = 1.
    gradevals = 0
    energy_evals = 0
    N=0;
    while dnorm > 1e-7: ## Stop if gradient gets bellow threshold.

        N+=1
        xGeom = np.append(xGeom,X) ## Save geometries.
        
        ## Bisection line search
        alpha, X_1, r1, evals = Bisection_search(d,X,neighbours,next_on_face, prev_on_face,k0,f0,0,0.00001,1000,1e-10,N,UseAll) 
        gradevals += evals

        ##Polak-Ribiére
        beta = max(PolakRibiere(r0,r1),0)


        #print(r1)
        
          ##Steepest descend
#         beta = 0 

          ##Fletcher-Reeves
#         beta = FletcherReeves(r0,r1)

        ## If energy is increased, start over with beta = 0.
        if energy(X_1,neighbours,k0,f0) > energy(X,neighbours,k0,f0):
            beta = 0  
            X_1  = X
            r1   = r0
        
        energy_evals += 2
        
        d = r1 + beta*d ## Conjugate the search direction
        d /= la.norm(d) #Normalizes new search direction
        
        
        r0 = r1 
        X = X_1
        
        
        ## If the number of itereations goes above 10*Natoms stop.
        dnorm = la.norm((r1))
        print(dnorm)

        if N == N_max*10: 
            #print(dnorm)
            print("GradEvals", gradevals)
            print("EnergyEvals", energy_evals) 
            return X, r1, N, xGeom 
            
        
    print("GradEvals", gradevals)
    print("EnergyEvals", energy_evals) 
    return X, r1, N, xGeom
start = time.time()
#X, d_end, N_itt, All_X  = conjugate_gradient(points_start, cubic_neighbours,next_on_face, prev_on_face,k0,f0, len(points_start),2)



start = time.time()
#X, d_end, N_itt, All_X  = conjugate_gradient(points_start, cubic_neighbours,next_on_face, prev_on_face,k0,f0, len(points_start),2)
end = time.time()
#np.save("X.npy",X)
X = np.load("X.npy").astype('float32')
print("Elapsed Time: ", end-start)


#for i in range(60):
 #   for j in range(3):
  #      print(X[i, j],"," ,end="")


energy(X,cubic_neighbours,k0,f0)
gradient(X, cubic_neighbours, next_on_face, prev_on_face, k0, f0, True)
#print(Aben)
#print(la.norm(Aben))
plotting_faces(Gtest,pentagons,hexagons,ap=0.3,ah=0.3,ax_off=True)



#print(G)

#print('Elapsed Time: ', time.time()-start)
#print(f'Energy end:    {energy(X,cubic_neighbours,k0,f0)}')
#print(f'gradient norm: {la.norm(d_end)}')
#print(f'Iterations until convergance: {N_itt}')







def binary(num):
    return ''.join('{:0>8b}'.format(c) for c in struct.pack('!d', num))


#print(G[28])
#print(Gtest[28])

#result = G - Gtest
