#include <chrono>
#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
using namespace std;

#include <array>
#include <tuple>
typedef double real_t;

/*********** DATA TYPES ***********/
#include "coord3d.cc"
typedef uint16_t              node_t;  // NB: Increase to int32 to do more than 65k atoms
template <int N> using CubicArcs = array<node_t, N*3>;



template <int N> class FullereneForcefieldEnergy {
public:

    /************** DATA *(************/
    const CubicArcs<N> neighbours; 
    array<coord3d,N>           X;      // Current nucleus positions
    const array<uint8_t, N*3>  face_left, face_right;
    
    // Optimal parameters. TODO: Fill out with the correct constants as constexpr's.
    array<real_t,2>          optimal_corner_cos_angles;	    // indexed by face size: {p,h}  {0,1}
    array<array<real_t,2>,2> optimal_bond_lengths; 	    // indexed by incident faces: {{pp,ph},{hp,pp}} 
    array<array<array<real_t,2>,2>,2> optimal_dih_cos_angles; // indexed by ... faces: {{{ppp,pph},{php,phh}}, {{hpp,hph},{hhp,hhh}}}  

    // Force constants. TODO: Add them to the code, both here as constexpr and use them down in the energy function
    real_t bond_force, angle_force, dih_force;

    /************** CONSTRUCTORS **************/
    FullereneForcefieldEnergy(const CubicArcs<N> &neighbours, const array<coord3d,N> &X, const array<uint8_t,N*3> &face_left, const array<uint8_t,N*3> &face_right) :
        neighbours(neighbours), X(X), face_left(face_left), face_right(face_right) {}
    


    };

}


int main()
{
  // TODO: Read in.
  CubicArcs<60>        neighbours;
  array<coord3d,60>    X0;
  array<uint8_t,3*60>  face_left, face_right;
  
  FullereneForcefieldEnergy<60> F(neighbours,X0,face_left,face_right);

  printf("%g\n",F.energy());	// Just to generate the code
  
  return 0;
}
