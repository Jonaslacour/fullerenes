#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>
#include "Gradient.cc"
#include <assert.h>
#include <stdio.h>
using namespace std;



namespace python_api { 
    namespace py = pybind11;
    typedef py::array_t<real_t, py::array::c_style> np_realarray;  
    
    void gradient(const np_realarray neighbours, const np_realarray X, const np_realarray face_right, const np_realarray next_on_face, const np_realarray prev_on_face, const np_realarray grad_container){
        auto neighbours_info = neighbours.request(); auto X_info = X.request(); auto face_right_info = face_right.request(); auto next_on_face_info = next_on_face.request(); auto prev_on_face_info = prev_on_face.request(); auto grad_info = grad_container.request();
        const int size = neighbours_info.shape;
        ::FullereneForcefield<size> forcefield = ::FullereneForcefield<size>(neighbours_info.ptr, X_info.ptr, face_right_info.ptr, next_on_face_info.ptr, prev_on_face_info.ptr);
        forcefield.gradient(X_info.ptr, grad_info.ptr);
    }

}


PYBIND11_MODULE(Gradient, m) {
    m.doc() = "Example Python Module in C++"; // optional module docstring
    m.def("gradient",       &python_api::gradient); // Har både input og output numpy arrays
    
}