
#include "cuda_runtime.h"
#include <cuda_runtime_api.h>
#include <cuda.h>
#include "device_launch_parameters.h"
#include <stdio.h>
#include <helper_cuda.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include "coord3d.cu"
#include "helper_functions.cu"

using namespace std::literals;
namespace cg = cooperative_groups;

typedef uint16_t node_t; 

__device__ struct ArcData{
    //124 FLOPs;
    __device__ ArcData(const node_t a, const uint8_t j, const coord3d* __restrict__ X, const BookkeepingData& bdat){   
        this->j = j;   

        real_t r_rmp;
        coord3d ap, am, ab, ac, ad, mp;
        coord3d X_a = X[a]; coord3d X_b = X[bdat.neighbours[j]];
        //printf("Index: %d \n", a*3 + j);

        //Compute the arcs ab, ac, ad, bp, bm, ap, am, mp, bc and cd
        ab = (X_b - X_a);  r_rab = bond_length(ab); ab_hat = r_rab * ab;
        ac = (X[bdat.neighbours[(j+1)%3]] - X_a); r_rac = bond_length(ac); ac_hat = r_rac * ac;
        ad = (X[bdat.neighbours[(j+2)%3]] - X_a); r_rad = bond_length(ad); ad_hat = r_rad * ad;
        
        coord3d bp = (X[bdat.next_on_face[j]] - X_b); bp_hat = unit_vector(bp);
        coord3d bm = (X[bdat.prev_on_face[j]] - X_b); bm_hat = unit_vector(bm);

        ap = bp + ab; r_rap = bond_length(ap); ap_hat = r_rap * ap;
        am = bm + ab; r_ram = bond_length(am); am_hat = r_ram * am;
        mp = bp - bm; r_rmp = bond_length(mp); mp_hat = r_rmp * mp;

        bc_hat = unit_vector(ac - ab);
        cd_hat = unit_vector(ad - ac);

        //Compute inverses of some arcs, these are subject to be omitted if the equations are adapted appropriately with inversion of signs.
        ba_hat = -ab_hat;
        mb_hat = -bm_hat;
        pa_hat = -ap_hat;
        pb_hat = -bp_hat;
    }

    //3 FLOPs
    __device__ real_t harmonic_energy(const real_t p0, const real_t p) const{
        return (real_t)0.5*(p-p0)*(p-p0);
    }
    //4 FLOPs
    __device__ coord3d  harmonic_energy_gradient(const real_t p0, const real_t p, const coord3d gradp) const{
        return (p-p0)*gradp;     
    }

    //1 FLOP
    __device__ real_t bond() const {return (real_t)1.0/r_rab;}

    //5 FLOPs
    __device__ real_t angle() const {return dot(ab_hat,ac_hat);}
    
    //Returns the inner dihedral angle for the current arc. Used here only for energy calculation, 
    //otherwise embedded in dihedral computation because the planes and angles that make up the dihedral angle computation are required for derivative computation.
    //50 FLOPs
    __device__ real_t dihedral() const 
    { 
        coord3d nabc, nbcd; real_t cos_b, cos_c, r_sin_b, r_sin_c;
        cos_b = dot(ba_hat,bc_hat); r_sin_b = rsqrt((real_t)1.0 - cos_b*cos_b); nabc = cross(ba_hat, bc_hat) * r_sin_b;
        cos_c = dot(-bc_hat,cd_hat); r_sin_c = rsqrt((real_t)1.0 - cos_c*cos_c); nbcd = cross(-bc_hat,cd_hat) * r_sin_c;
        return dot(nabc, nbcd);
    }
    
    // Chain rule terms for angle calculation
    //Computes gradient related to bending term. ~24 FLOPs
    __device__ coord3d inner_angle_gradient(const ArcConstants& c) const
    {   
        real_t cos_angle = angle(); //Inner angle of arcs ab,ac.
        coord3d grad = cos_angle * (ab_hat * r_rab + ac_hat * r_rac) - ab_hat * r_rac - ac_hat* r_rab; //Derivative of inner angle: Eq. 21. 
        return c.f_inner_angle * harmonic_energy_gradient(c.angle0, cos_angle, grad); //Harmonic Energy Gradient: Eq. 21. multiplied by harmonic term.
    }
    //Computes gradient related to bending of outer angles. ~20 FLOPs
    __device__ coord3d outer_angle_gradient_m(const ArcConstants& c) const
    {
        real_t cos_angle = -dot(ab_hat, bm_hat); //Compute outer angle. ab,bm
        coord3d grad = (bm_hat + ab_hat * cos_angle) * r_rab; //Derivative of outer angles Eq. 30. Buster Thesis
        return c.f_outer_angle_m * harmonic_energy_gradient(c.outer_angle_m0,cos_angle,grad); //Harmonic Energy Gradient: Eq. 30 multiplied by harmonic term.
    }
    __device__ coord3d outer_angle_gradient_p(const ArcConstants& c) const
    {
        real_t cos_angle = -dot(ab_hat, bp_hat); //Compute outer angle. ab,bp
        coord3d grad = (bp_hat + ab_hat * cos_angle) * r_rab; //Derivative of outer angles Eq. 28. Buster Thesis
        return c.f_outer_angle_p * harmonic_energy_gradient(c.outer_angle_p0,cos_angle,grad); //Harmonic Energy Gradient: Eq. 28 multiplied by harmonic term.
    }
    // Chain rule terms for dihedral calculation
    //Computes gradient related to dihedral/out-of-plane term. ~75 FLOPs
    __device__ coord3d inner_dihedral_gradient(const ArcConstants& c) const
    {
        coord3d nabc, nbcd; real_t cos_b, cos_c, r_sin_b, r_sin_c;
        cos_b = dot(ba_hat,bc_hat); r_sin_b = rsqrtf((real_t)1.0 - cos_b*cos_b); nabc = cross(ba_hat, bc_hat) * r_sin_b;
        cos_c = dot(-bc_hat,cd_hat); r_sin_c = rsqrtf((real_t)1.0 - cos_c*cos_c); nbcd = cross(-bc_hat,cd_hat) * r_sin_c;

        real_t cos_beta = dot(nabc, nbcd); //Inner dihedral angle from planes abc,bcd.
        real_t cot_b = cos_b * r_sin_b * r_sin_b; //cos(b)/sin(b)^2

        //Derivative w.r.t. inner dihedral angle F and G in Eq. 26
        coord3d grad = cross(bc_hat, nbcd) * r_sin_b * r_rab - ba_hat * cos_beta * r_rab + (cot_b * cos_beta * r_rab) * (bc_hat - ba_hat * cos_b);

        return c.f_inner_dihedral * harmonic_energy_gradient(c.inner_dih0, cos_beta, grad); //Eq. 26.
    }

    //Computes gradient from dihedral angles constituted by the planes bam, amp ~162 FLOPs
    __device__ coord3d outer_a_dihedral_gradient(const ArcConstants& c) const
    {
        coord3d nbam_hat, namp_hat; real_t cos_a, cos_m, r_sin_a, r_sin_m;

        cos_a = dot(ab_hat,am_hat); r_sin_a = rsqrtf((real_t)1.0 - cos_a*cos_a); nbam_hat = cross(ab_hat,am_hat) * r_sin_a;
        cos_m = dot(-am_hat,mp_hat); r_sin_m = rsqrtf((real_t)1.0 - cos_m*cos_m); namp_hat = cross(-am_hat,mp_hat) * r_sin_m;
        
        real_t cos_beta = dot(nbam_hat, namp_hat); //Outer Dihedral angle bam, amp
        real_t cot_a = cos_a * r_sin_a * r_sin_a;
        real_t cot_m = cos_m * r_sin_m * r_sin_m;

        //Derivative w.r.t. outer dihedral angle, factorized version of Eq. 31.
        coord3d grad = cross(mp_hat,nbam_hat)*r_ram*r_sin_m - (cross(namp_hat,ab_hat)*r_ram + cross(am_hat,namp_hat)*r_rab)*r_sin_a +
                        cos_beta*(ab_hat*r_rab + r_ram * ((real_t)2.0*am_hat + cot_m*(mp_hat+cos_m*am_hat)) - cot_a*(r_ram*(ab_hat - am_hat*cos_a) + r_rab*(am_hat-ab_hat*cos_a)));
        
        //Eq. 31 multiplied by harmonic term.
        return c.f_outer_dihedral * harmonic_energy_gradient(c.outer_dih0, cos_beta, grad);
    }

    //Computes gradient from dihedral angles constituted by the planes nbmp, nmpa ~92 FLOPs
    __device__ coord3d outer_m_dihedral_gradient(const ArcConstants& c) const
    {
        coord3d nbmp_hat, nmpa_hat; real_t cos_m, cos_p, r_sin_m, r_sin_p;
        cos_m = dot(mb_hat,mp_hat); r_sin_m = rsqrtf((real_t)1.0 - cos_m*cos_m); nbmp_hat = cross(mb_hat,mp_hat) * r_sin_m;
        cos_p = dot(-mp_hat,pa_hat); r_sin_p = rsqrtf((real_t)1.0 - cos_p*cos_p); nmpa_hat = cross(-mp_hat,pa_hat) * r_sin_p;
        
        //Cosine to the outer dihedral angle constituted by the planes bmp and mpa
        real_t cos_beta = dot(nbmp_hat, nmpa_hat); //Outer dihedral angle bmp,mpa.
        real_t cot_p = cos_p * r_sin_p * r_sin_p;
        
        //Derivative w.r.t. outer dihedral angle, factorized version of Eq. 32.
        coord3d grad = r_rap * (cot_p*cos_beta * (-mp_hat - pa_hat*cos_p) - cross(nbmp_hat, mp_hat)*r_sin_p - pa_hat*cos_beta );

        //Eq. 32 multiplied by harmonic term.
        return c.f_outer_dihedral * harmonic_energy_gradient(c.outer_dih0, cos_beta, grad);
    }

    //Computes gradient from dihedral angles constituted by the planes bpa, pam ~162 FLOPs
    __device__ coord3d outer_p_dihedral_gradient(const ArcConstants& c) const
    {
        coord3d nbpa_hat, npam_hat; real_t cos_p, cos_a, r_sin_p, r_sin_a;
        cos_a = dot(ap_hat,am_hat); r_sin_a = rsqrtf((real_t)1.0 - cos_a*cos_a); npam_hat = cross(ap_hat,am_hat) * r_sin_a;
        cos_p = dot(pb_hat,-ap_hat); r_sin_p = rsqrtf((real_t)1.0 - cos_p*cos_p); nbpa_hat = cross(pb_hat,-ap_hat) * r_sin_p;

        real_t cos_beta = dot(nbpa_hat, npam_hat); //Outer dihedral angle bpa, pam.
        real_t cot_p = cos_p * r_sin_p * r_sin_p;
        real_t cot_a = cos_a * r_sin_a * r_sin_a;

        //Derivative w.r.t. outer dihedral angle, factorized version of Eq. 33.
        coord3d grad = cross(npam_hat,pb_hat)*r_rap*r_sin_p - (cross(am_hat,nbpa_hat)*r_rap + cross(nbpa_hat,ap_hat)*r_ram)*r_sin_a +
                        cos_beta*(am_hat*r_ram + r_rap * ((real_t)2.0*ap_hat + cot_p*(pb_hat+cos_p*ap_hat)) - cot_a*(r_rap*(am_hat - ap_hat*cos_a) + r_ram*(ap_hat-am_hat*cos_a)));
        
        //Eq. 33 multiplied by harmonic term.
        return c.f_outer_dihedral * harmonic_energy_gradient(c.outer_dih0, cos_beta, grad);
    }
    // Internal coordinate gradients
    __device__ coord3d bond_length_gradient(const ArcConstants& c) const { return - c.f_bond * harmonic_energy_gradient(c.r0,bond(),ab_hat);}
    //Sum of angular gradient components.
    __device__ coord3d angle_gradient(const ArcConstants& c) const { return inner_angle_gradient(c) + outer_angle_gradient_p(c) + outer_angle_gradient_m(c);}
    //Sum of inner and outer dihedral gradient components.
    __device__ coord3d dihedral_gradient(const ArcConstants& c) const { return inner_dihedral_gradient(c) + outer_a_dihedral_gradient(c) + outer_m_dihedral_gradient(c) + outer_p_dihedral_gradient(c);}
    //coord3d flatness()             const { return ;  }   
    
    //Harmonic energy contribution from bond stretching, angular bending and dihedral angle bending.
    //71 FLOPs
    __device__ real_t energy(const ArcConstants& c) const {return (real_t)0.5 *c.f_bond *harmonic_energy(bond(),c.r0)+ c.f_inner_angle* harmonic_energy(angle(),c.angle0) + c.f_inner_dihedral* harmonic_energy(dihedral(),c.inner_dih0);}
    //Sum of bond, angular and dihedral gradient components.
    __device__ coord3d gradient(const ArcConstants& c) const{return bond_length_gradient(c) + angle_gradient(c) + dihedral_gradient(c);}
    
    uint8_t j;

    //Residual lengths of arcs ab, ac, am, ap.
    real_t
        r_rab,
        r_rac,
        r_rad,
        r_ram,
        r_rap;

    //Base Arcs,
    coord3d
        ab,
        ac,
        ad;

    /*
    All normalized arcs required to perform energy & gradient calculations.
    Note that all these arcs are cyclical the arc ab becomes: ab->ac->ad,  the arc ac becomes: ac->ad->ab , the arc bc becomes: bc->cd->db (For iterations 0, 1, 2)
    As such the naming convention here is related to the arcs as they are used in the 0th iteration. */
    coord3d 
        ab_hat,
        ac_hat,
        ad_hat,
        bp_hat,
        bm_hat,
        am_hat,
        ap_hat,
        ba_hat,
        bc_hat,
        cd_hat,
        mp_hat,
        mb_hat,
        pa_hat,
        pb_hat;
};

__device__ coord3d gradient(const coord3d* __restrict__ X, const node_t node_id, const uint8_t arc_id, const BookkeepingData &dat, const ArcConstants &constants) {
    ArcData arc = ArcData::ArcData(node_id, arc_id, X, dat);
    coord3d grad = arc.gradient(constants);
    return grad;
}

__device__ real_t energy(const coord3d* __restrict__ X, const node_t node_id, const uint8_t arc_id, const BookkeepingData &dat, const ArcConstants &constants, real_t* __restrict__ reduction_array, real_t* __restrict__ gdata, const node_t N, bool single_block_fullerenes) {
    //(71 + 124) * 3 * N  = 585*N FLOPs
    ArcData arc = ArcData::ArcData(node_id, arc_id, X, dat);
    real_t arc_energy = arc.energy(constants);
    
    cg::sync(cg::this_thread_block());
    reduction_array[threadIdx.x] = arc_energy;
    // (/N // 32) * log2(32) = N//32  * 5 FLOPs 
    reduction(reduction_array,gdata, N*3, single_block_fullerenes); 
    return reduction_array[0];
}

__device__ void golden_section_search(coord3d* __restrict__ X, coord3d& direction, coord3d& new_direction,coord3d* __restrict__ X1, coord3d* __restrict__ X2, real_t* __restrict__ reduction_array, real_t* __restrict__ gdata, const node_t node_id, const uint8_t arc_id, const node_t N, const BookkeepingData& dat, const ArcConstants& constants, cg::thread_group sync_group, bool single_block_fullerenes){
    real_t tau = (sqrtf(5) - 1) / 2;
    //Actual coordinates resulting from each traversal 
    //Line search x - values;
    real_t a = 0.0; real_t b = 1.0;
    real_t x1,  x2, dfc;


    x1 = (a + (1 - tau) * (b - a));
    x2 = (a + tau * (b - a));
    
    X1[node_id] = X[node_id]; X2[node_id] = X[node_id];

    atomic_add(X1[node_id], x1*direction);
    atomic_add(X2[node_id], x2*direction);


    cg::sync(sync_group);

    real_t f1 = energy(X1, node_id, arc_id, dat, constants, reduction_array, gdata, N, single_block_fullerenes);
    real_t f2 = energy(X2, node_id, arc_id, dat, constants, reduction_array, gdata, N, single_block_fullerenes);

    for (uint8_t i = 0; i < 20; i++){
        if (f1 > f2){
            a = x1;
            x1 = x2;
            f1 = f2;
            x2 = a + tau * (b - a);
            cg::sync(sync_group);
            X2[node_id] = X[node_id]; atomic_add(X2[node_id],x2*direction);
            cg::sync(sync_group);
            f2 = energy(X2, node_id, arc_id, dat, constants, reduction_array, gdata, N, single_block_fullerenes);
        }else
        {
            b = x2;
            x2 = x1;
            f2 = f1;
            x1 = a + (1 - tau) * (b - a);
            cg::sync(sync_group);
            X1[node_id] = X[node_id]; atomic_add(X1[node_id],x1*direction);
            cg::sync(sync_group);
            f1 = energy(X1, node_id, arc_id, dat, constants, reduction_array, gdata, N, single_block_fullerenes);
        }
    }
    //Line search coefficient
    real_t alfa = (a+b)/2;
    cg::sync(sync_group);
    X1[node_id] = X[node_id]; atomic_add(X1[node_id], alfa*direction);
    cg::sync(sync_group);
    new_direction = -gradient(X1,node_id, arc_id, dat, constants);
}

__global__ void conjugate_gradient(coord3d* d_X, coord3d* d_X_temp, coord3d* d_X2, const node_t* d_neighbours, const node_t* d_next_on_face, const node_t* d_prev_on_face, const uint8_t* d_face_right, real_t* gdata, const size_t N, const bool single_block_fullerenes){
    extern __shared__ real_t smem[];
    

    cg::grid_group grid = cg::this_grid();
    uint8_t arc_id = threadIdx.x%3;

    coord3d* sX;
    coord3d* sX_temp;
    coord3d* sX2;

    coord3d delta_x0, delta_x1, direction;
    node_t node_id;
    size_t offset;

    size_t iter_count = 0;
    size_t max_iter = N*2.6;
    size_t gradient_evals = 0;
    size_t energy_evals = 0;
    
    real_t beta, dnorm, r0_norm, direction_norm;
    beta = dnorm = r0_norm = direction_norm = 0.0;

    //If fullerenes are localized to individual blocks then use block threadIdx else use grid ID and use 1 grid per fullerene with concurrent launches.
    //If fullerenes are localized to individual blocks then use block size to determine pointer in array, else assume concurrent kernel launches with pointers to individual fullerenes.
    if (single_block_fullerenes){
        node_id = (uint16_t)floor(threadIdx.x/3) ;
        offset = blockIdx.x * N;
    } else
    {
        node_id = (uint16_t)((blockDim.x * blockIdx.x + threadIdx.x)/3);
        offset = 0;
    }
    
    coord3d* X = &d_X[offset];
    coord3d* X_temp = &d_X_temp[offset];
    coord3d* X2 = &d_X2[offset];
    
    if (single_block_fullerenes)
    {
        sX =&reinterpret_cast<coord3d*>(smem)[N + 1];
        sX_temp =&reinterpret_cast<coord3d*>(smem)[N + 2 + N];
        sX2 =&reinterpret_cast<coord3d*>(smem)[N + 3 +2*N];  
        sX[node_id] = X[node_id];
        sX_temp[node_id] = sX[node_id];

        X = &sX[0]; X_temp = &sX_temp[0]; X2 = &sX2[0];
    } else {
        X_temp[node_id] = X[node_id];
    }

    
    //Pre-compute force constants and store in registers.
    BookkeepingData bookit = BookkeepingData::BookkeepingData(&d_neighbours[3*offset],&d_face_right[3*offset],&d_next_on_face[3*offset],&d_prev_on_face[3*offset]);
    ArcConstants constants = compute_arc_constants(bookit, node_id, arc_id);

    //Load constant bookkeeping data into registers.
    const node_t neighbours[3] = {d_neighbours[3*(offset+node_id)],d_neighbours[3*(offset+node_id) + 1],d_neighbours[3*(offset+node_id) + 2]};
    const uint8_t face_right[3] = {d_face_right[3*(offset+node_id)],d_face_right[3*(offset+node_id) + 1],d_face_right[3*(offset+node_id) + 2]};;
    const node_t next_on_face[3] = {d_next_on_face[3*(offset+node_id)],d_next_on_face[3*(offset+node_id) + 1],d_next_on_face[3*(offset+node_id) + 2]};
    const node_t prev_on_face[3] = {d_prev_on_face[3*(offset+node_id)],d_prev_on_face[3*(offset+node_id) + 1],d_prev_on_face[3*(offset+node_id) + 2]};
    BookkeepingData bookkeeping = BookkeepingData::BookkeepingData(&neighbours[0],&face_right[0],&next_on_face[0],&prev_on_face[0]);   

    cg::sync(grid);
    direction = gradient(X, node_id, arc_id, bookkeeping, constants);
    gradient_evals ++;
    
    smem[threadIdx.x] = dot(direction,direction);

    reduction(smem,gdata,N*3,single_block_fullerenes);
    dnorm = sqrtf(smem[0]);
    direction = -direction/dnorm;
    

    delta_x0 = direction;
    
    for (size_t i = 0; i < max_iter; i++)
    {   
        beta = 0.0; direction_norm = 0.0; dnorm=0.0; r0_norm = 0.0;
        cg::sync(grid);
        if (single_block_fullerenes){golden_section_search(X, direction, delta_x1, X_temp, X2, smem, gdata, node_id, arc_id, N, bookkeeping, constants, cg::this_thread_block(), single_block_fullerenes);} 
        else { golden_section_search(X, direction, delta_x1, X_temp, X2, smem, gdata, node_id, arc_id, N, bookkeeping, constants, cg::this_grid(), single_block_fullerenes);}
        
        
        cg::sync(grid);

        gradient_evals++;
        energy_evals += 42;
        //Polak Ribiere method
        
        smem[threadIdx.x] = dot(delta_x0, delta_x0); reduction(smem,gdata,N*3,single_block_fullerenes); r0_norm = smem[0];
        cg::sync(grid);
        smem[threadIdx.x] = dot(delta_x1, (delta_x1 - delta_x0)); reduction(smem,gdata,N*3,single_block_fullerenes); beta = smem[0] / r0_norm;
        cg::sync(grid);
       
        if (energy(X_temp, node_id, arc_id, bookkeeping, constants, smem, gdata, N, single_block_fullerenes) > energy(X, node_id, arc_id, bookkeeping, constants, smem, gdata, N, single_block_fullerenes))
        {   
            cg::sync(grid);
            X_temp[node_id] =  X[node_id];
            delta_x1 =  delta_x0;
            beta = 0.0;
        }
        else
        {   
            cg::sync(grid);
            X[node_id] = X_temp[node_id];
            delta_x0 = delta_x1;
        }
        direction = delta_x1 + beta*direction;
        //Calculate gradient and residual gradient norms..
        cg::sync(grid);
        smem[threadIdx.x] = dot(direction,direction); 
        cg::sync(grid);
        reduction(smem,gdata,N*3,single_block_fullerenes); 
        cg::sync(grid);
        direction_norm = sqrtf(smem[0]);
        cg::sync(grid);
        smem[threadIdx.x] = dot(delta_x1,delta_x1); 
        cg::sync(grid);
        reduction(smem,gdata,N*3,single_block_fullerenes);
        cg::sync(grid);
        dnorm = sqrtf(smem[0]);
        cg::sync(grid);
        //Normalize gradient.
        direction /= direction_norm;
        iter_count++;
        if ( (threadIdx.x + blockDim.x*blockIdx.x) == 0 )
        {
            //printf("%e \n", dnorm);
        }
        
    }
    
    cg::sync(grid);
    real_t test = energy(X_temp, node_id, arc_id, bookkeeping, constants, smem, gdata, N, single_block_fullerenes);
    cg::sync(grid);
    
    if ((node_id == 0))
    {
        printf("Energy at end %e \n", test);
        /* code */
    }
}

void callKernelMultiBlockFullerenes(real_t* h_X, node_t* h_cubic_neighbours, node_t* h_next_on_face, node_t* h_prev_on_face, uint8_t* h_face_right, const size_t N){
    
    cudaDeviceProp properties;
    cudaGetDeviceProperties(&properties, 0);
    size_t blocksize = optimize_block_size(N,properties,conjugate_gradient);
    dim3 dimBlock = dim3::dim3(blocksize, 1, 1);
    dim3 dimGrid = dim3::dim3(ceil(N/blocksize), 1, 1);

    std::cout << "Grid Dimension: " << dimBlock.x << " , " << dimGrid.x << "\n" ;
    bool single_block_fullerenes = false;
    //batch_size = floor((GPU_properties.maxThreadsPerBlock*GPU_properties.multiProcessorCount)/(dimBlock.x*dimGrid.x) );
    //GPU_properties.asyncEngineCount;
    size_t* d_N;
    bool* d_single_block_fullerenes;


    coord3d* d_X;
    coord3d* d_X_temp;
    coord3d* d_X2;
    coord3d* d_delta_x0;
    coord3d* d_delta_x1;
    coord3d* d_direction;

    node_t* d_neighbours;
    uint8_t* d_face_right;
    node_t* d_next_on_face;
    node_t* d_prev_on_face;
    real_t* d_gdata;

    cudaError_t error;
    error = cudaMalloc(&d_X, sizeof(coord3d)*N);
    error = cudaMalloc(&d_X_temp, sizeof(coord3d)*N);
    error = cudaMalloc(&d_X2, sizeof(coord3d)*N);
    
    error = cudaMalloc(&d_neighbours, sizeof(node_t)*3*N);
    error = cudaMalloc(&d_next_on_face, sizeof(node_t)*3*N);
    error = cudaMalloc(&d_prev_on_face, sizeof(node_t)*3*N);
    error = cudaMalloc(&d_face_right, sizeof(uint8_t)*3*N);
    error = cudaMalloc(&d_gdata, sizeof(real_t)*dimGrid.x);
    error = cudaMalloc(&d_N, sizeof(size_t)); cudaMemcpy(d_N, &N, sizeof(size_t), cudaMemcpyHostToDevice);
    error = cudaMalloc(&d_single_block_fullerenes, sizeof(bool)); cudaMemcpy(d_single_block_fullerenes, &single_block_fullerenes, sizeof(bool), cudaMemcpyHostToDevice);

    getLastCudaError("One or more Mallocs Failed! \n");

    error = cudaMemcpy(d_X, h_X, sizeof(coord3d)*N , cudaMemcpyHostToDevice);
    error = cudaMemcpy(d_neighbours, h_cubic_neighbours, sizeof(node_t)*3*N, cudaMemcpyHostToDevice);
    error = cudaMemcpy(d_next_on_face, h_next_on_face, sizeof(node_t)*3*N, cudaMemcpyHostToDevice);
    error = cudaMemcpy(d_prev_on_face, h_prev_on_face, sizeof(node_t)*3*N, cudaMemcpyHostToDevice);
    error = cudaMemcpy(d_face_right, h_face_right, sizeof(uint8_t)*3*N, cudaMemcpyHostToDevice);

    getLastCudaError("Memcpy Failed! \n");

    cudaDeviceSynchronize();
    auto start = std::chrono::system_clock::now();
    void* kernelArgs[] = {
        (void*)&d_X,
        (void*)&d_X_temp,
        (void*)&d_X2,
        (void*)&d_neighbours,
        (void*)&d_next_on_face,
        (void*)&d_prev_on_face,
        (void*)&d_face_right,
        (void*)&d_gdata,
        (void*)&N,
        (void*)&single_block_fullerenes
    };
    checkCudaErrors(cudaLaunchCooperativeKernel((void*)conjugate_gradient, dimGrid, dimBlock, kernelArgs, sizeof(real_t)*dimBlock.x, NULL));

    cudaDeviceSynchronize();
    auto end = std::chrono::system_clock::now();
    std::cout << "Elapsed time: " << (end-start)/ 1ms << "ms\n" ;
    getLastCudaError("Failed to launch kernel: ");


}

void callKernelSingleBlockFullerenes(real_t* h_X, node_t* h_cubic_neighbours, node_t* h_next_on_face, node_t* h_prev_on_face, uint8_t* h_face_right, size_t N, const size_t batch_size){
    bool concurrent_kernels = false;
    bool single_block_fullerenes = true;


    dim3 dimBlock = dim3::dim3(N*3, 1, 1);
    dim3 dimGrid = dim3::dim3(batch_size, 1, 1);

    /*
    cudaDeviceProp GPU_properties;
    cudaGetDeviceProperties(&GPU_properties,0);
    bool use_L1_cache = (GPU_properties.sharedMemPerBlock > (sharedMemoryPerBlock ) && (maxActiveBlocks > 0) );
    std::cout << use_L1_cache << "\n";
    bool single_block_fullerenes = maxActiveBlocks > 0;
    size_t batch_size = maxActiveBlocks*GPU_properties.multiProcessorCount;
    dim3 dimBlock, dimGrid;
    
    
    if (!concurrent_kernels)
    {
        
        
    } else
    {
        size_t blocksize = optimize_block_size(N,GPU_properties,conjugate_gradient);
        dimBlock = dim3::dim3(blocksize, 1, 1);
        dimGrid = dim3::dim3(ceil(N/blocksize), 1, 1);
        batch_size = floor((GPU_properties.maxThreadsPerBlock*GPU_properties.multiProcessorCount)/(dimBlock.x*dimGrid.x) );

        GPU_properties.asyncEngineCount;
        
    }

    if (!use_L1_cache) {sharedMemoryPerBlock = sizeof(real_t)*dimBlock.x*2;}
    std::cout << dimBlock.x << "\n";
    std::cout << dimGrid.x << "\n";


    */


    size_t* d_N;
    bool* d_single_block_fullerenes;


    coord3d* d_X;
    coord3d* d_X_temp;
    coord3d* d_X2;
    coord3d* d_delta_x0;
    coord3d* d_delta_x1;
    coord3d* d_direction;

    node_t* d_neighbours;
    uint8_t* d_face_right;
    node_t* d_next_on_face;
    node_t* d_prev_on_face;
    real_t* d_gdata;

    cudaError_t error;
    error = cudaMalloc(&d_X, sizeof(coord3d)*N*batch_size);
    error = cudaMalloc(&d_X_temp, sizeof(coord3d)*N*batch_size);
    error = cudaMalloc(&d_X2, sizeof(coord3d)*N*batch_size);
    
    error = cudaMalloc(&d_neighbours, sizeof(node_t)*3*N*batch_size);
    error = cudaMalloc(&d_next_on_face, sizeof(node_t)*3*N*batch_size);
    error = cudaMalloc(&d_prev_on_face, sizeof(node_t)*3*N*batch_size);
    error = cudaMalloc(&d_face_right, sizeof(uint8_t)*3*N*batch_size);
    error = cudaMalloc(&d_gdata, sizeof(real_t)*dimGrid.x);
    error = cudaMalloc(&d_N, sizeof(size_t)); cudaMemcpy(d_N, &N, sizeof(size_t), cudaMemcpyHostToDevice);
    error = cudaMalloc(&d_single_block_fullerenes, sizeof(bool)); cudaMemcpy(d_single_block_fullerenes, &single_block_fullerenes, sizeof(bool), cudaMemcpyHostToDevice);

    getLastCudaError("One or more Mallocs Failed! \n");

    error = cudaMemcpy(d_X, h_X, sizeof(coord3d)*N*batch_size , cudaMemcpyHostToDevice);
    error = cudaMemcpy(d_neighbours, h_cubic_neighbours, sizeof(node_t)*3*N*batch_size, cudaMemcpyHostToDevice);
    error = cudaMemcpy(d_next_on_face, h_next_on_face, sizeof(node_t)*3*N*batch_size, cudaMemcpyHostToDevice);
    error = cudaMemcpy(d_prev_on_face, h_prev_on_face, sizeof(node_t)*3*N*batch_size, cudaMemcpyHostToDevice);
    error = cudaMemcpy(d_face_right, h_face_right, sizeof(uint8_t)*3*N*batch_size, cudaMemcpyHostToDevice);

    getLastCudaError("Memcpy Failed! \n");

    auto start = std::chrono::system_clock::now();
    
    if (!concurrent_kernels)
    {
        void* kernelArgs[] = {
        (void*)&d_X,
        (void*)&d_X_temp,
        (void*)&d_X2,
        (void*)&d_neighbours,
        (void*)&d_next_on_face,
        (void*)&d_prev_on_face,
        (void*)&d_face_right,
        (void*)&d_gdata,
        (void*)&N,
        (void*)&single_block_fullerenes
        };
        checkCudaErrors(cudaLaunchCooperativeKernel((void*)conjugate_gradient, dimGrid, dimBlock, kernelArgs, sizeof(coord3d)*4*(N+1), NULL));
    } 
    
    
    /*
    else
    {   
        cudaStream_t stream[batch_size];
        for (size_t i = 0; i < batch_size; i++)
        {
            cudaStreamCreate(&stream[i]);
        }
        
        for (size_t i = 0; i < 16; i++)
        {   
            d_X = &d_X[i*N]; d_X_temp = &d_X_temp[i*N]; d_X2 = &d_X2[i*N]; d_neighbours = &d_neighbours[i*N*3]; d_next_on_face = &d_next_on_face[i*N*3]; d_prev_on_face = &d_prev_on_face[i*N*3];
            d_face_right = &d_face_right[i*N*3]; d_gdata = &d_gdata[i];

            void* kernelArgs[] = {
            (void*)&d_X,
            (void*)&d_X_temp,
            (void*)&d_X2,
            (void*)&d_neighbours,
            (void*)&d_next_on_face,
            (void*)&d_prev_on_face,
            (void*)&d_face_right,
            (void*)&d_gdata,
            (void*)&N,
            (void*)&single_block_fullerenes
        };
            checkCudaErrors(cudaLaunchCooperativeKernel((void*)conjugate_gradient, dimGrid, dimBlock, kernelArgs, sharedMemoryPerBlock, stream[i]));
        }
    }
    */


    cudaDeviceSynchronize();
    auto end = std::chrono::system_clock::now();


    
    std::cout << "Elapsed time: " << (end-start)/ 1ms << "ms\n" ;
    getLastCudaError("Failed to launch kernel: ");

}


