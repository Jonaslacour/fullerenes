import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams as rc
from cycler import cycler
from numpy.core.numeric import full_like
from scipy import optimize as opt

from mpl_toolkits.axes_grid.inset_locator import (inset_axes, InsetPosition,
                                                 mark_inset)
#import scipy.optimize as opt
#from ExternalFunctions import Chi2Regression, nice_string_output, add_text_to_ax

c1 = [(76,114,176), (85,168,104), (196,78,82), (129,114,175), (204,185,116), (100,181,205)]
c = []
for i in range(6):
    c.append((np.round(c1[i][0]/255,2), np.round(c1[i][1]/255,2), np.round(c1[i][2]/255,2)))

for i in range(5):
    a0 = np.round(np.linspace(c1[i][0],c1[i+1][0],4)/255,2)
    a1 = np.round(np.linspace(c1[i][1],c1[i+1][1],5)/255,2)
    a2 = np.round(np.linspace(c1[i][2],c1[i+1][2],5)/255,2)
    c[6+(2*i):6+2+(2*i)] = [(a0[1],a1[1],a2[1]), (a0[2],a1[2],a2[2])]

colorfullcolors = c

rc["legend.markerscale"] = 2.0
rc["legend.framealpha"] = 0
rc["legend.labelspacing"] = 0.1
rc['axes.prop_cycle'] = cycler(color=colorfullcolors)

rc['axes.autolimit_mode'] = 'data'
rc['axes.xmargin'] = 0
rc['axes.ymargin'] = 0.10
rc['font.sans-serif'] = "Times New Roman"
rc['font.serif'] = "Times New Roman"
rc['xtick.labelsize'] = 9.5
rc['ytick.labelsize'] = 9.5
rc['axes.grid'] = False
rc['grid.linestyle'] = '-'
rc['grid.alpha'] = 0.2

from mpl_toolkits.mplot3d import Axes3D
import matplotlib

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{:.2f}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

KernelNames = ['Naive CUDA', 'Local Gradient', 'Precompute Constants', 'Shared Memory', 'Vectorized Loads', 'Segmented']
RegisterPressure = [160, 156, 128, 166, 167, 168]

Path = '/Users/jonaslacour/Documents/fullerenes/Results/'

Python = np.loadtxt(Path + 'Python.txt', delimiter=",")
Seq = np.loadtxt(Path + 'Seq.txt', delimiter=",")
Kernel = np.loadtxt(Path + 'Kernel0.txt', delimiter=",")
Local = np.loadtxt(Path + 'Local0.txt', delimiter=",")
Precompute = np.loadtxt(Path + 'Precompute0.txt', delimiter=",")
Shared0 = np.loadtxt(Path + 'Shared0.txt', delimiter=",")
Concurrent = np.loadtxt(Path + 'Concurrent.txt', delimiter=",")

Concurrent[:,2] /= 3.3333

Shared64 = np.loadtxt(Path + 'Shared64.txt', delimiter=",")
Shared72 = np.loadtxt(Path + 'Shared72.txt', delimiter=",")
Shared80 = np.loadtxt(Path + 'Shared80.txt', delimiter=",")
Shared88 = np.loadtxt(Path + 'Shared88.txt', delimiter=",")
Shared96 = np.loadtxt(Path + 'Shared96.txt', delimiter=",")
Shared104 = np.loadtxt(Path + 'Shared104.txt', delimiter=",")
Shared112 = np.loadtxt(Path + 'Shared112.txt', delimiter=",")
Shared120 = np.loadtxt(Path + 'Shared120.txt', delimiter=",")
Shared128 = np.loadtxt(Path + 'Shared128.txt', delimiter=",")

def FLOPS_PER_SECOND(data,C, label=None, handle=None, FLOPS = None):
    
    N = data[:,0]
    Batch = data[:,1]
    T = data[:,2]
    if FLOPS is None:
        FLOPS = (411*N*Batch*3*N*22  + 2106*N*Batch*3*N)/T
    if handle is not None:
        handle.plot(data[:,0],FLOPS,'.',linewidth=1, markersize=2, color=C, label=label)
        handle.plot(data[:,0],FLOPS,'-',linewidth=1, markersize=2,alpha=0.3, color=C) 


def FLOPS(data):
    N = data[:,0]
    Batch = data[:,1]
    T = data[:,2]
    return (411*N*Batch*3*N*22  + 2106*N*Batch*3*N)/T

def FULLERENES_PER_SECOND(data,C, label=None, handle=None):
    if handle is not None:
        handle.plot(data[:,0],data[:,1]/data[:,2],'.',linewidth=1, markersize=2, color=C, label=label) 
        handle.plot(data[:,0],data[:,1]/data[:,2],'-',linewidth=1, markersize=2,alpha=0.3, color=C) 
    


fig, ax1 = plt.subplots()
FLOPS_PER_SECOND(Python, 'C0', 'Python', ax1, (411*Python[:,0]*Python[:,4] +  2106*Python[:,0]*Python[:,3])/Python[:,2])
FLOPS_PER_SECOND(Seq, 'C1', 'C++', ax1)
FLOPS_PER_SECOND(Kernel, 'C2', 'Naive CUDA', ax1)
FLOPS_PER_SECOND(Local, 'C3', 'Local Gradient', ax1)
FLOPS_PER_SECOND(Precompute ,'C4','Precompute Constants', ax1)
FLOPS_PER_SECOND(Shared0, 'C5', 'Shared Memory', ax1)
ax1.legend(loc='best')
ax1.set_xlabel('Isomer Space, N')
ax1.set_ylabel('FLOPS')
plt.yscale('log')


fig, ax2 = plt.subplots()
FULLERENES_PER_SECOND(Python, 'C0', 'Python', ax2)
FULLERENES_PER_SECOND(Seq, 'C1', 'C++', ax2)
FULLERENES_PER_SECOND(Kernel, 'C2', 'Naive CUDA', ax2)
FULLERENES_PER_SECOND(Local, 'C3', 'Local Gradient', ax2)
FULLERENES_PER_SECOND(Precompute ,'C4','Precompute Constants', ax2)
FULLERENES_PER_SECOND(Shared0, 'C5', 'Shared Memory', ax2)
ax2.legend(loc='best')
ax2.set_xlabel('Isomer Space, N')
ax2.set_ylabel('Fullerenes Per Second')
plt.yscale('log')

fig, ax3 = plt.subplots()
FLOPS_PER_SECOND(Shared64, 'C0', 'Registers: 64', ax3)
FLOPS_PER_SECOND(Shared72, 'C1', 'Registers: 72', ax3)
FLOPS_PER_SECOND(Shared80, 'C2', 'Registers: 80', ax3)
FLOPS_PER_SECOND(Shared88, 'C3', 'Registers: 88', ax3)
FLOPS_PER_SECOND(Shared96, 'C4', 'Registers: 96', ax3)
FLOPS_PER_SECOND(Shared104, 'C5', 'Registers: 104', ax3)
FLOPS_PER_SECOND(Shared112, 'C6', 'Registers: 112', ax3)
FLOPS_PER_SECOND(Shared120, 'C7', 'Registers: 120', ax3)
FLOPS_PER_SECOND(Shared128, 'C8', 'Registers: 128', ax3)

ax3.legend(loc='best')
ax3.set_xlabel('Isomer Space, N')
ax3.set_ylabel('FLOPS [1/s]')
ax3.set_title('Optimal Regcount')


fig, ax4 = plt.subplots()
FLOPS_PER_SECOND(Python, 'C0', 'Python', ax4, (411*Python[:,0]*Python[:,4] +  2106*Python[:,0]*Python[:,3])/Python[:,2])
FLOPS_PER_SECOND(Seq, 'C1', 'C++', ax4)
FLOPS_PER_SECOND(Kernel, 'C2', 'Naive CUDA', ax4)
FLOPS_PER_SECOND(Local, 'C3', 'Local Gradient', ax4)
FLOPS_PER_SECOND(Precompute ,'C4','Precompute Constants', ax4)
FLOPS_PER_SECOND(Shared0, 'C5', 'Shared Memory', ax4)
FLOPS_PER_SECOND(Shared64, 'C6', 'Shared Memory 64', ax4)
FLOPS_PER_SECOND(Concurrent, 'C7', 'Concurrent', ax4)

ax4.legend(loc='lower left')
ax4.set_xlabel('Isomer Space, N')
ax4.set_ylabel('FLOPS [1/s]')
plt.yscale('log')

ax5 = plt.axes([0,0,1,1])
ip = InsetPosition(ax4, [0.4,0.1,0.5,0.5])
ax5.set_axes_locator(ip)
ax5.set_yscale('log')
mark_inset(ax4, ax5, loc1=2, loc2=4, fc="none", ec='0.5')


FLOPS_PER_SECOND(Precompute[50:] ,'C4','Precompute Constants', ax5)
FLOPS_PER_SECOND(Shared0[50:], 'C5', 'Shared Memory', ax5)
FLOPS_PER_SECOND(Shared64[50:], 'C6', 'Shared Memory 64', ax5)
FLOPS_PER_SECOND(Concurrent[35:], 'C7', 'Concurrent', ax5)



max_FLOPS = [np.max(FLOPS(Seq)),np.max(FLOPS(Kernel)), np.max(FLOPS(Local)), np.max(FLOPS(Precompute)), np.max(FLOPS(Shared0))]
avg_FLOPS = [np.mean(FLOPS(Seq[10:])), np.mean(FLOPS(Kernel[10:])), np.mean(FLOPS(Local[10:])), np.mean(FLOPS(Precompute[10:])), np.mean(FLOPS(Shared0[10:]))]
print(max_FLOPS)
print(avg_FLOPS)


"""
#Python
GradEvals 191877
EnergyEvals 10240
Elapsed Time:  959.2165746688843

#C++
Solving 1 fullerenes of size: 512 Elapsed time: 29918ms
Estimated Performance 9.76767e+08FLOP/s 

#Kernel
Solving 136 fullerenes of size: 512 Elapsed time: 35727ms
Estimated Performance 1.11244e+11FLOP/s

#Local Gradient
Solving 68 fullerenes of size: 512 Elapsed time: 12738ms
Estimated Performance 1.56004e+11FLOP/s 

#Precompute State
Solving 136 fullerenes of size: 512 Elapsed time: 345ms
Estimated Performance 3.44831e+12FLOP/s 

#Shared
Solving 68 fullerenes of size: 512 
Elapsed time: 294ms
Estimated Performance 6.75351e+12FLOP/s 

#Shared64
Solving 136 fullerenes of size: 512 Elapsed time: 552ms
Estimated Performance 7.1893e+12FLOP/s 

#Concurrent
Solving 640 fullerenes of size: 512 Elapsed time: 2448ms
Estimated Performance 7.63812e+12FLOPS 
"""

C512_Times = [29.91, 35.727, 12.738, 0.345, 0.294, 0.552, 2448]
BatchSize = [1, 136, 68, 136, 68, 136,  68, 640]
C512_FLOPS = [191877*2106 + 10240*411 ,(411*512*BatchSize*10*512*22  + 2106*512*BatchSize*10*512)/C512_Times]

#plt.figure()
#fig, ax = plt.subplots()

#rects = ax.bar(1,C512_FLOPS, 1, label='Sequential')






plt.show()

