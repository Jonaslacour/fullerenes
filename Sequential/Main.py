import unittest 
import numpy as np
import numpy.linalg as la
from numpy import array, pi, cos, sin, sqrt, linspace
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


import time

from Fullerene_GeometryFunctions import *


from IPython.core.display import display, HTML
display(HTML("<style>.container { width:100% !important; }</style>")) 

np.random.seed(22)

NA = np.newaxis;

from C60ih import *
#print(points_opt.shape)
#print(cubic_neighbours.shape)
#print(pentagons.shape)
#print(hexagons.shape)


#print('DFT m062x optimized geometry')

## General force parameters
R0,ang0,dih0,ang0_p,ang0_m,dih_a,dih_m,dih_p,fR,fang,fdih,fang_p,fang_m,fdih_a,fdih_m,fdih_p = Parameters_from_geometry(face_right,cubic_neighbours,False)

## Seminario Force Parameters, for C60Ih
# R0,ang0,dih0,ang0_p,ang0_m,dih_a,dih_m,dih_p,fR,fang,fdih,fang_p,fang_m,fdih_a,fdih_m,fdih_p = Parameters_from_geometry(face_right,cubic_neighbours,True)

k0 = array([R0,ang0,dih0,ang0_p,ang0_m,dih_a,dih_m,dih_p])
f0 = array([fR,fang,fdih,fang_p,fang_m,fdih_a,fdih_m,fdih_p])

def conjugate_gradient(X, neighbours,next_on_face, prev_on_face,k0,f0, N_max,UseAll): 

    d = -gradient(X,neighbours,next_on_face, prev_on_face,k0,f0,UseAll)
    r0 = d; #initial residual 
    d /= la.norm(d) # Normalized initial search direction
    
    #Save geometries for plotting
    xGeom = np.empty(0)  
    print(X.shape)
    print(neighbours.shape)
    print(next_on_face.shape)
    print(prev_on_face.shape)
    print(k0.shape)
    print(f0.shape)
    
    dnorm = la.norm(d) #initial dnorm = 1.
    
    N=0;
    while dnorm > 1e-7: ## Stop if gradient gets bellow threshold.

        N+=1
        xGeom = np.append(xGeom,X) ## Save geometries.
        
        ## Bisection line search
        alpha, X_1, r1 = Bisection_search(d,X,neighbours,next_on_face, prev_on_face,k0,f0,0,0.00001,1000,1e-10,N,UseAll) 
            
        ##Polak-Ribiére
        beta = max(PolakRibiere(r0,r1),0)

          ##Steepest descend
#         beta = 0 

          ##Fletcher-Reeves
#         beta = FletcherReeves(r0,r1)

        ## If energy is increased, start over with beta = 0.
        if energy(X_1,neighbours,k0,f0) > energy(X,neighbours,k0,f0):
            beta = 0  
            X_1  = X
            r1   = r0

        
        d = r1 + beta*d ## Conjugate the search direction
        d /= la.norm(d) #Normalizes new search direction
        
        
        r0 = r1 
        X = X_1
        
        
        ## If the number of itereations goes above 10*Natoms stop.
        dnorm = la.norm((r1))
        if N == N_max*10: 
            print(dnorm)
            return X, r1, N, xGeom  
        
        
    return X, r1, N, xGeom
start = time.time()
#X, d_end, N_itt, All_X  = conjugate_gradient(points_start, cubic_neighbours,next_on_face, prev_on_face,k0,f0, len(points_start),2)
#print('Elapsed Time: ', time.time()-start)
#print(f'Energy end:    {energy(X,cubic_neighbours,k0,f0)}')
#print(f'gradient norm: {la.norm(d_end)}')
#print(f'Iterations until convergance: {N_itt}')



ab = np.array([1,1,0.1])
bm = np.array([0,1,0.1])
bp = np.array([1,0,0.1])
ab_hat = ab/bond_length(ab)
bm_hat = bm/bond_length(bm)
bp_hat = bp/bond_length(bp)

result = outer_dih_a_gradient_single_vector(1,2,bond_length(ab),ab,bm,bp,ab_hat,bp_hat,bm_hat)

print(result)

#plotting_faces(X,pentagons,hexagons,ap=0.3,ah=0.3,ax_off=True)
